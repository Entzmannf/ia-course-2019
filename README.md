# README ia-course-2019

## Utilisation directe 

Avec le lien suivant, vous disposez d'un environnement non-persistant :

https://mybinder.org/v2/gl/utt-connected-innovation%2Fia-course-2019/master


## Créer votre environnement personnel

Le dépôt git original est accessible à l'adresse : https://gitlab.com/utt-connected-innovation/ia-course-2019

Vous devez avoir un compte gitlab

Vous devez **forker** le dépôt git original :

![fork.png](media/fork.png)

Voici comment il apparait une fois **forké** :

![forked.png](media/forked.png)


### Accès direct via URL mybinder

Remplacer dans l'URL suivante **XXXX** par votre identifiant gitlab : 
https://mybinder.org/v2/gl/XXXX%2Fia-course-2019/master

Par exemple : 
https://mybinder.org/v2/gl/Entzmannf%2Fia-course-2019/master


### Accès UI mybinder

Vous devez créer votre espace mybinder sur https://mybinder.org/ en choisissant **gitlab.com** comme source
et en indiquant l'URL de votre dépôt gitlab 

![binder.png](media/binder.png)

cliquer sur launch ...

![binder2.png](media/binder2.png)

celà peut prendre quelques minutes la première fois :

![binder3.png](media/binder3.png)

![binder4.png](media/binder4.png)

L'environnement jupyter notebook apparait :

![ready.png](media/ready.png)


## TODO

```
git remote add parent https://gitlab.com/utt-connected-innovation/ia-course-2019
```

Vous devez faire des git commit/push

```
git status
git add .
git commit -am 'before merge'
git push
```

Pour récupérer les modifs du dépôt original :

```
git commit -am 'before merge'
git pull parent master --strategy-option theirs
git commit -m 'merge'
git push
```


# TODO


```
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```
